import './style.css';
import {Map,View} from 'ol';
import OSM from 'ol/source/OSM.js';
import Static from 'ol/source/ImageStatic.js';
import proj4 from 'proj4';
import {Image as ImageLayer, Tile as TileLayer} from 'ol/layer.js';
import {getCenter} from 'ol/extent.js';
import {register} from 'ol/proj/proj4.js';
import {transform} from 'ol/proj.js';

proj4.defs(
    'EPSG:32614',
    '+proj=utm +zone=14 +datum=WGS84 +units=m +no_defs +type=crs'
);
register(proj4);

const imageExtent = [510000, 2100000, 460000, 2200000];
const imageLayer = new ImageLayer();

const map = new Map({
  layers: [
    new TileLayer({
      source: new OSM(),
    }),
    imageLayer,
  ],
  target: 'map',
  view: new View({
    center: transform(getCenter(imageExtent), 'EPSG:32614', 'EPSG:3857'),
    zoom: 7,
  }),
});

const interpolate = document.getElementById('interpolate');

function setSource() {
  const source = new Static({
    url:
      'https://scontent.fmex34-1.fna.fbcdn.net/v/t39.30808-6/279576964_5059541004163301_6272163703774268073_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=dd5e9f&_nc_ohc=NvD1UIW_c-QAX8A4Wex&_nc_ht=scontent.fmex34-1.fna&oh=00_AfBzzwxkJpo34mbA0fldZu4awMl_3nY5PJ7Sjs_zV9owpg&oe=65DDA70C',
    crossOrigin: '',
    projection: 'EPSG:32614',
    imageExtent: imageExtent,
    interpolate: interpolate.checked,
  });
  imageLayer.setSource(source);
}
setSource();

interpolate.addEventListener('change', setSource);